//
//  ListItem.swift
//  MyCloudList
//
//  Created by Thomas Irgang on 26.10.18.
//  Copyright © 2018 Thomas Irgang. All rights reserved.
//

import Foundation
import UIKit

protocol ListItemRO {
    func getTitle() -> String
    func getDue() -> Date?
    func getIcon() -> String
    func getDone() -> Bool
    func getAssignee() -> String?
    func getAssets() -> [Asset]
}

protocol ListItemRW: ListItemRO {
    func update(title: String) -> Bool
    func update(due: Date?) -> Bool
    func update(icon: String) -> Bool
    func update(done: Bool) -> Bool
    func update(assignee: String?) -> Bool
}

extension ListItem: ListItemRW {
    
    func update(title: String) -> Bool {
        if title.count > 0 {
            self.title = title
            return true
        }
        return false
    }
    
    func update(due: Date?) -> Bool {
        self.due = due
        return true
    }
    
    func update(icon: String) -> Bool {
        if UIImage(named: icon) != nil {
            self.icon = icon
            return true
        }
        return false
    }
    
    func update(done: Bool) -> Bool {
        self.done = done
        return true
    }
    
    func update(assignee: String?) -> Bool {
        self.assignee = assignee
        return true
    }
    
    func getTitle() -> String {
        return self.title ?? "Title"
    }
    
    func getDue() -> Date? {
        return self.due
    }
    
    func hasDue() -> Bool {
        return self.due != nil
    }
    
    func getIcon() -> String {
        return self.icon ?? "default"
    }
    
    func getDone() -> Bool {
        return self.done
    }
    
    func getAssignee() -> String? {
        return self.assignee
    }
    
    func hasAssignee() -> Bool {
        return self.assignee != nil
    }
    
    func getAssets() -> [Asset] {
        if let assets = self.assets as? Set<Asset> {
            return Array(assets)
        }
        return []
    }
}
