//
//  ListItemTableCellModel.swift
//  MyCloudList
//
//  Created by Thomas Irgang on 26.10.18.
//  Copyright © 2018 Thomas Irgang. All rights reserved.
//

import Foundation
import UIKit

class ListItemTableCellModel {
    
    public var item: ListItemRO?
    public var dateFormat = "dd.MM.yyyy"
    
    private var formatter: DateFormatter
    
    init() {
        self.formatter = DateFormatter()
        self.formatter.dateFormat = dateFormat
    }
    
    public func getTitle() -> String {
        return item?.getTitle() ?? ""
    }
    
    public func getDueDate() -> String {
        if let due = item?.getDue() {
            return formatter.string(from: due)
        } else {
            return ""
        }
    }
    
    public func getAssignee() -> String {
        return item?.getAssignee() ?? ""
    }
    
    public func getItemIcon() -> UIImage? {
        if let name = item?.getIcon() {
            return UIImage(named: name)
        }
        return nil
    }
    
    public func getAssetsIcon() -> UIImage? {
        let assets = item?.getAssets() ?? []
        var icon = "no_assets"
        if assets.count == 1 {
            icon = "one_asset"
        } else if assets.count > 1 {
            icon = "more_asset"
        }
        return UIImage(named: icon)
    }
    
    public func getStatusIcon() -> UIImage? {
        if let done = item?.getDone() {
            let icon = done ? "done" : "open"
            return UIImage(named: icon)
        }
        return nil
    }
}
