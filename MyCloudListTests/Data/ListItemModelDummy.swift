//
//  ListItemModelDummy.swift
//  MyCloudListTests
//
//  Created by Thomas Irgang on 26.10.18.
//  Copyright © 2018 Thomas Irgang. All rights reserved.
//

import Foundation
@testable import MyCloudList

class ListItemModelDummy: ListItemRO {
    var title: String = "Item Title"
    var due: Date? = Date(timeIntervalSince1970: 0)
    var icon: String = "default"
    var done: Bool = false
    var assignee: String = "me"
    var assets: [Asset] = []
    
    func getTitle() -> String {
        return title
    }
    
    func getDue() -> Date? {
        return due
    }
    
    func getIcon() -> String {
        return icon
    }
    
    func getDone() -> Bool {
        return done
    }
    
    func getAssignee() -> String? {
        return assignee
    }
    
    func getAssets() -> [Asset] {
        return assets
    }
}
