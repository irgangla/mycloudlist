//
//  ListItemTableCellModel.swift
//  MyCloudListTests
//
//  Created by Thomas Irgang on 26.10.18.
//  Copyright © 2018 Thomas Irgang. All rights reserved.
//

import XCTest
@testable import MyCloudList

class ListItemTableCellModelTest: XCTestCase {

    private var cell: ListItemTableCellModel!
    private var model: ListItemModelDummy!
    
    override func setUp() {
        cell = ListItemTableCellModel()
        model = ListItemModelDummy()
        cell.item = model
    }

    override func tearDown() {
    }

    func testTitle() {
        XCTAssertEqual(cell.getTitle(), model.getTitle())
    }
    
    func testDue() {
        XCTAssertEqual(cell.getDueDate(), "01.01.1970")
    }

    func testItemIcon() {
        XCTAssertEqual(cell.getItemIcon(), UIImage(named: model.getIcon()))
    }

    func testAssetIcon() {
        let assets = model.getAssets()
        var icon = "no_assets"
        if assets.count == 1 {
            icon = "one_asset"
        } else if assets.count > 1 {
            icon = "more_asset"
        }
        XCTAssertEqual(cell.getAssetsIcon(), UIImage(named: icon))
    }
    
    func testStatusIcon() {
        let icon = model.getDone() ? "done" : "open"
        XCTAssertEqual(cell.getStatusIcon(), UIImage(named: icon))
    }
    
    func testAssignee() {
        cell.item = nil
        XCTAssertEqual(cell.getAssignee(), "")
    }
    
    func testTitleNoModel() {
        cell.item = nil
        XCTAssertEqual(cell.getTitle(), "")
    }
    
    func testDueNoModel() {
        cell.item = nil
        XCTAssertEqual(cell.getDueDate(), "")
    }
    
    func testItemIconNoModel() {
        cell.item = nil
        XCTAssertEqual(cell.getItemIcon(), nil)
    }
    
    func testAssetIconNoModel() {
        cell.item = nil
        let icon = "no_assets"
        XCTAssertEqual(cell.getAssetsIcon(), UIImage(named: icon))
    }
    
    func testStatusIconNoModel() {
        cell.item = nil
        XCTAssertEqual(cell.getStatusIcon(), nil)
    }
    
    func testAssigneeNoModel() {
        cell.item = nil
        XCTAssertEqual(cell.getAssignee(), "")
    }
    
    func testItemIconUnknownAsset() {
        model.icon = "invalidIconName"
        XCTAssertEqual(cell.getItemIcon(), nil)
    }
    
    func testStatusIconDone() {
        model.done = true
        let icon = "done"
        XCTAssertEqual(cell.getStatusIcon(), UIImage(named: icon))
    }
    
    func testStatusIconOpen() {
        model.done = false
        let icon = "open"
        XCTAssertEqual(cell.getStatusIcon(), UIImage(named: icon))
    }
    
    func testNoDueDate() {
        model.due = nil
        XCTAssertEqual(cell.getDueDate(), "")
    }
}
